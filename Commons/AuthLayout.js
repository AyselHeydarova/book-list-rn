import React from "react";
import {
    StyleSheet, 
    TouchableWithoutFeedback, 
    StatusBar, 
    View,
    Text,
    Image
} from 'react-native';
import { LinearGradient } from "expo-linear-gradient";
import COLORS from '../Styles/colors';


export const AuthLayout = ({children, coverImg}) => (
    <TouchableWithoutFeedback>
        <View style={styles.container}>
            <StatusBar
                backgroundColor="transparent"
                translucent={true}
                />
             <LinearGradient
                colors = {[COLORS.main, COLORS.secondary]}
                style = {styles.gradient}/>
            <View>
                <View style={styles.imageWrapper}>
                     <Image resizeMode="contain"  style={styles.image} source={coverImg} />
                </View>
               

                <View>
                    {children}
                </View>
        
            </View>
     </View>
     </TouchableWithoutFeedback>
     )

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: "center",
        paddingHorizontal: "15%",
        backgroundColor: "orange",
    },

    gradient: {
       ...StyleSheet.absoluteFill,
    },

    image: {
        width: 180,
        height: 190
    },

    imageWrapper: {
        alignItems: 'center'
    }

})

