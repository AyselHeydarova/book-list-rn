const COLORS = {
    main: "#28345D",
    secondary:"#868FB8",
    light: "#CAD2ED",
    btn: "#616DA3",
    add1: "#FFDDD7",
    add2: "#FBA454", 
};

export default COLORS;