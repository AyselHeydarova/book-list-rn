import React from 'react';
import {StyleSheet, View, Text, Button} from 'react-native';
import { AuthLayout } from '../Commons/AuthLayout';
import welcomeImg from '../assets/welcome.png'
import { CustomBtn } from '../components/CustomBtn';


export const WelcomeScreen = ()=> {
    return (
        
        <AuthLayout coverImg={welcomeImg}>
                <CustomBtn title="login" 
                            style={styles.btn}/>
                <CustomBtn title="Sign up"
                            style={styles.btn}/>
        </AuthLayout>

    )
}

const styles = StyleSheet.create(
    {
        container: {
            flex: 1,
            justifyContent:"center",
            alignItems:'center'
        },
        btn: {
            marginTop: 15
        }
    }
)