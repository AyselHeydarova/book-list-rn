import React from 'react';
import {
    StyleSheet,
  View,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
  Text
} from 'react-native';

import COLORS from '../Styles/colors';

export const CustomBtn = ({title, onPress, style, ...rest}) => {
 
    const Touchable = 
    Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;
    return (
        <View style = {[meymun.container, style]}>
            <Touchable >
            <View style={meymun.btn}>
                 <Text style={meymun.title}>{title}</Text>
            </View>

        </Touchable>
        </View>
        
    )
}; 

const meymun = StyleSheet.create({
    container: {
        overflow: "hidden",
        borderRadius: 50

    },
    btn: {
        width: "100%",
        backgroundColor: COLORS.btn,
        padding: 15,
        alignItems: 'center',
        borderRadius: 50
       
    },

    title: {
        color: "white",
        textAlign: "center",
        textTransform: "uppercase"
    }
});